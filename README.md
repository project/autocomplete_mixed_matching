# Autocomplete widget with mixed matching

This module provides a widget that is similar to the autocomplete widget from Drupal core.

However, to determine the results, it does two queries:
- First it finds matches that start with the search term.
- If not enough matches were found, the rest is filled with matches that contain the search term.


## Development setup

The instructions below assume that [ddev](https://ddev.com/get-started/) is available on your development machine.

```sh
# Run these steps from within the module directory, after git clone.
ddev start
ddev composer install
# Create symlinks in ./web/modules/custom/autocomplete_mixed_matching/.
# This command needs to run again whenever a new file or directory is added in
# the root directory.
ddev symlink-project
```

### Run the tests

```sh
ddev phpcs
ddev exec ./vendor/bin/phpstan
ddev phpunit
```

### Demo site

Optionally, install a demo site based on Umami profile.

```sh
ddev drush si demo_umami -y --account-pass=admin --site-name="Autocomplete mixed matching demo"
ddev drush pmu -y update
ddev drush en -y autocomplete_mixed_matching
# Create example fields.
ddev drush field:create node recipe --field-name=field_other_tag --field-label="Other tag" --field-type=entity_reference --field-widget=autocomplete_mixed_matching --target-type=taxonomy_term --target-bundle=tags -y --cardinality=1
ddev drush field:create node recipe --field-name=field_other_recipe --field-label="Other recipe" --field-type=entity_reference --field-widget=autocomplete_mixed_matching --target-type=node --target-bundle=recipe -y --cardinality=1
# Create a login link.
ddev drush uli
```

The site is available at https://drupal-autocomplete-mixed-matching.ddev.site/.

To see the widget in action:
- Visit https://drupal-autocomplete-mixed-matching.ddev.site/en/node/add/recipe to create a new recipe.
- Find the "Other tag" field, type "party".
  - The autocomplete list shows "Party" at the top, and "Cocktail party" below, defying alphabetic order.
- Find the "Other recipe" field, type "th".
  - The autocomplete list shows "Thai green curry" above "Borscht with pork ribs", defying alphabetic order.
  - _Add more recipes to see more relevant examples._
  - _Note that, with a multilingual site, autocomplete matching and sorting does consider the titles in all languages, even if they are not displayed._
