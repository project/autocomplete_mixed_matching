<?php

declare(strict_types=1);

namespace Drupal\Tests\autocomplete_mixed_matching\Kernel;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Site\Settings;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\system\Controller\EntityAutocompleteController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tests entity autocomplete requests.
 *
 * @see \Drupal\KernelTests\Core\Entity\EntityAutocompleteTest
 */
class AutocompleteUrlKernelTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   *
   * @var list<string>
   */
  protected static $modules = [
    'entity_reference_test',
    'autocomplete_mixed_matching',
  ];

  /**
   * The entity type used in this test.
   *
   * @var string
   */
  protected $entityType = 'entity_test';

  /**
   * The bundle used in this test.
   *
   * @var string
   */
  protected $bundle = 'entity_test';

  /**
   * Tests autocompletion edge cases with slashes in the names.
   */
  public function testAutocompleteResultMatching(): void {
    $names = [
      'Cyan frog',
      'White frog',
      'Frogs wear pyjamas',
      'Frog',
      'Toad',
    ];
    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
    $storage = $this->container->get('entity_type.manager')->getStorage($this->entityType);
    $entities = [];
    foreach ($names as $name) {
      $entity = $storage->create(['name' => $name]);
      $entity->save();
      $entities[$name] = $entity;
    }

    $this->assertAutocompleteResultsLabels(
      $contains_labels = [
        // Letter 'C' is before 'F' in the alphabet.
        'Cyan frog',
        // Letter 'W' is after 'F' in the alphabet.
        'White frog',
        'Frogs wear pyjamas',
        'Frog',
      ],
      'frog',
      $entities,
    );

    // Verify that 'CONTAINS' is the default.
    $this->assertAutocompleteResultsLabels(
      $contains_labels,
      'frog',
      $entities,
      [
        'match_operator' => 'CONTAINS',
      ],
    );

    // Test 'CONTAINS' + sort.
    $this->assertAutocompleteResultsLabels(
      [
        'Cyan frog',
        'Frog',
        'Frogs wear pyjamas',
        'White frog',
      ],
      'frog',
      $entities,
      [
        'match_operator' => 'CONTAINS',
        'sort' => [
          'field' => 'name',
          'direction' => 'ASC',
        ],
      ],
    );

    // Test 'CONTAINS' + sort + limit.
    $this->assertAutocompleteResultsLabels(
      [
        'Cyan frog',
        'Frog',
        'Frogs wear pyjamas',
      ],
      'frog',
      $entities,
      [
        'match_operator' => 'CONTAINS',
        'sort' => [
          'field' => 'name',
          'direction' => 'ASC',
        ],
        'match_limit' => 3,
      ],
    );

    // Test 'Starts with' matching.
    $this->assertAutocompleteResultsLabels(
      [
        'Frogs wear pyjamas',
        'Frog',
      ],
      'frog',
      $entities,
      [
        'match_operator' => 'STARTS_WITH',
      ],
    );

    // Test starts with + sort.
    $this->assertAutocompleteResultsLabels(
      [
        'Frog',
        'Frogs wear pyjamas',
      ],
      'frog',
      $entities,
      [
        'match_operator' => 'STARTS_WITH',
        'sort' => [
          'field' => 'name',
          'direction' => 'ASC',
        ],
      ],
    );

    // Test mixed matching.
    $this->assertAutocompleteResultsLabels(
      [
        'Frogs wear pyjamas',
        'Frog',
        'Cyan frog',
        'White frog',
      ],
      'frog',
      $entities,
      [
        'match_operator' => 'STARTS_WITH',
        'fallback_match_operator' => 'CONTAINS',
      ],
    );

    // Test mixed matching + sort.
    $this->assertAutocompleteResultsLabels(
      [
        'Frog',
        'Frogs wear pyjamas',
        'Cyan frog',
        'White frog',
      ],
      'frog',
      $entities,
      [
        'match_operator' => 'STARTS_WITH',
        'fallback_match_operator' => 'CONTAINS',
        'sort' => [
          'field' => 'name',
          'direction' => 'ASC',
        ],
      ],
    );

    // Test mixed matching + sort + limit.
    $this->assertAutocompleteResultsLabels(
      [
        'Frog',
        'Frogs wear pyjamas',
        'Cyan frog',
      ],
      'frog',
      $entities,
      [
        'match_operator' => 'STARTS_WITH',
        'fallback_match_operator' => 'CONTAINS',
        'sort' => [
          'field' => 'name',
          'direction' => 'ASC',
        ],
        'match_limit' => 3,
      ],
    );
  }

  /**
   * Fetches autocomplete results, asserts integrity, and asserts the labels.
   *
   * @param list<string> $expected_labels
   *   Labels expected in the response.
   * @param string $input
   *   Search input.
   * @param array $entities
   *   The entities that were created for this test.
   * @param array $selection_settings
   *   Selection settings.
   */
  protected function assertAutocompleteResultsLabels(array $expected_labels, string $input, array $entities, array $selection_settings = []): void {
    $labels = $this->getAutocompleteResultsLabels($input, $entities, $selection_settings);
    $this->assertSame($expected_labels, $labels);
  }

  /**
   * Fetches autocomplete results, asserts integrity, and returns the labels.
   *
   * @param string $input
   *   Search input.
   * @param array $entities
   *   The entities that were created for this test.
   * @param array $selection_settings
   *   Selection settings.
   *
   * @return list<string>
   *   Labels from the autocomplete response.
   */
  protected function getAutocompleteResultsLabels(string $input, array $entities, array $selection_settings = []): array {
    $result = $this->fetchAutocompleteResult($input, $selection_settings, NULL);
    $this->assertIsArray($result);
    $this->assertTrue(\array_is_list($result));
    $labels = [];
    foreach ($result as $item) {
      $label = $item['label'] ?? $this->fail("Item without a label.");
      $entity = $entities[$label] ?? $this->fail("Unknown entity '$label'.");
      $this->assertSame([
        'value' => $label . ' (' . $entity->id() . ')',
        'label' => $label,
      ], $item);
      $labels[] = $label;
    }
    return $labels;
  }

  /**
   * Returns the result of an Entity reference autocomplete request.
   *
   * @param string $input
   *   Search input.
   * @param array $selection_settings
   *   The label of the entity to query by.
   * @param int|null $entity_id
   *   Selection settings.
   *
   * @return list<array{value: string, label: string}>
   *   The JSON value encoded in its appropriate PHP type.
   */
  protected function fetchAutocompleteResult(string $input, array $selection_settings = [], int|null $entity_id = NULL): array {
    // Use "entity_test_all_except_host" EntityReferenceSelection
    // to also test passing an entity to autocomplete requests.
    $request = Request::create('entity_reference_autocomplete/' . $this->entityType . '/entity_test_all_except_host');
    $request->query->set('q', $input);
    if ($entity_id) {
      $request->query->set('entity_type', $this->entityType);
      $request->query->set('entity_id', $entity_id);
    }

    $selection_settings_key = Crypt::hmacBase64(serialize($selection_settings) . $this->entityType . 'entity_test_all_except_host', Settings::getHashSalt());
    \Drupal::keyValue('entity_autocomplete')->set($selection_settings_key, $selection_settings);

    $entity_reference_controller = EntityAutocompleteController::create($this->container);

    /** @var string $result_json */
    $result_json = $entity_reference_controller->handleAutocomplete($request, $this->entityType, 'entity_test_all_except_host', $selection_settings_key)->getContent();

    /** @var list<array{value: string, label: string}> $result */
    $result = Json::decode($result_json);

    return $result;
  }

}
