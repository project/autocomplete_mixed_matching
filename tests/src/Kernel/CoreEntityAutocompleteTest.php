<?php

declare(strict_types=1);

namespace Drupal\Tests\autocomplete_mixed_matching\Kernel;

use Drupal\KernelTests\Core\Entity\EntityAutocompleteTest;

/**
 * Test to verify that the core autocomplete still works.
 */
class CoreEntityAutocompleteTest extends EntityAutocompleteTest {

  /**
   * {@inheritdoc}
   *
   * @var list<string>
   */
  protected static $modules = [
    'entity_reference_test',
    'autocomplete_mixed_matching',
  ];

}
