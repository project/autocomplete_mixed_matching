<?php

declare(strict_types=1);

namespace Drupal\Tests\autocomplete_mixed_matching\FunctionalJavascript;

/**
 * Causes a test fail if errors are logged during the test run.
 *
 * With regular Drupal web/functional tests, it can happen that errors occur in
 * the Drupal site, but the test still passes, depending how the assertions are
 * written. Or, even if the test fails, the actual errors are not reported as
 * the reason for the failure.
 *
 * With this trait, such errors are always reported in the test output.
 */
trait FailOnErrorLogTrait {

  /**
   * {@inheritdoc}
   */
  protected function runTest(): mixed {
    $result = NULL;
    $failure = NULL;
    try {
      $result = parent::runTest();
    }
    catch (\Throwable $e) {
      $failure = $e;
    }
    // Check if errors were logged, no matter if the test failed or not.
    $error_log_file = $this->root . '/' . $this->siteDirectory . '/error.log';
    if (file_exists($error_log_file)) {
      $error_log_contents = file_get_contents($error_log_file);
      throw new \Exception(
        'Errors were logged during the test run:' . "\n" . $error_log_contents,
        // Make sure both exceptions are shown in the output.
        previous: $failure,
      );
    }
    if ($failure !== NULL) {
      throw $failure;
    }
    return $result;
  }

}
