<?php

declare(strict_types=1);

namespace Drupal\Tests\autocomplete_mixed_matching\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\EntityReference\EntityReferenceAutocompleteWidgetTest;

/**
 * This test verifies that the core widget still works as expected.
 */
class CoreWidgetTest extends EntityReferenceAutocompleteWidgetTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field_ui',
    'entity_test',
    'entity_reference_test',
    // Also enable this module.
    'autocomplete_mixed_matching',
  ];

  use FailOnErrorLogTrait;

}
