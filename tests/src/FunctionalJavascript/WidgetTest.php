<?php

declare(strict_types=1);

namespace Drupal\Tests\autocomplete_mixed_matching\FunctionalJavascript;

use Behat\Mink\Element\NodeElement;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\user\Entity\User;

/**
 * Tests the mixed matching autocomplete widget.
 *
 * @method assertSession(string $name = NULL): \Drupal\FunctionalJavascriptTests\WebDriverWebAssert
 *
 * @see \Drupal\autocomplete_mixed_matching\Plugin\Field\FieldWidget\MixedMatchingAutocompleteWidget
 * @see \Drupal\FunctionalJavascriptTests\EntityReference\EntityReferenceAutocompleteWidgetTest
 */
class WidgetTest extends WebDriverTestBase {

  use ContentTypeCreationTrait;
  use EntityReferenceFieldCreationTrait;
  use NodeCreationTrait;
  use FailOnErrorLogTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field_ui',
    'entity_test',
    'entity_reference_test',
    'autocomplete_mixed_matching',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a Content type and two test nodes.
    $this->createContentType(['type' => 'page']);

    // Create nodes in random-ish order, not alphabetic.
    $names = [
      // Add one item that would be last in alphabetic order.
      // Insert this first, to prove that the order of insertion has no effect.
      'Wood frog',
      // Add items that begin with the search term 'Frog'.
      'Frog with a funny hat',
      // Add one exact match.
      'Frog',
      // Add one item that does not contain the search term 'Frog'.
      'Toad',
      // Add some items that would be first in alphabetic order.
      '007 frog',
      'Acrobatic frog',
    ];
    foreach ($names as $name) {
      $this->createNode(['title' => $name]);
    }

    $user = $this->drupalCreateUser([
      'access content',
      'create page content',
    ]);
    $this->drupalLogin($user);
  }

  /**
   * Tests that the default autocomplete widget return the correct results.
   */
  public function testEntityReferenceAutocompleteWidget(): void {
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $display_repository */
    $display_repository = \Drupal::service('entity_display.repository');

    // Create an entity reference field and use the default 'CONTAINS' match
    // operator.
    $field_name = 'field_test';
    $this->createEntityReferenceField(
      'node',
      'page',
      $field_name,
      $field_name,
      'node',
      'default',
      [
        'target_bundles' => ['page'],
        'sort' => ['field' => 'title', 'direction' => 'ASC'],
      ],
    );
    $form_display = $display_repository->getFormDisplay('node', 'page');
    $form_display->setComponent($field_name, [
      'type' => 'autocomplete_mixed_matching',
    ]);
    // To satisfy config schema, the size setting must be an integer, not just
    // a numeric value. See https://www.drupal.org/node/2885441.
    $this->assertIsInt($form_display->getComponent($field_name)['settings']['size'] ?? $this->fail());
    $form_display->save();
    $this->assertIsInt($form_display->getComponent($field_name)['settings']['size'] ?? $this->fail());

    // Visit the node add page.
    $this->drupalGet('node/add/page');
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    $autocomplete_field = $assert_session->waitForElement('css', '[name="' . $field_name . '[0][target_id]"].ui-autocomplete-input');
    $autocomplete_field->setValue('Frog');
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $assert_session->waitOnAutocomplete();

    $this->assertAutocompleteMatches([
      'Frog',
      'Frog with a funny hat',
      '007 frog',
      'Acrobatic frog',
      'Wood frog',
    ]);

    // Change the size of the result set.
    $display_repository->getFormDisplay('node', 'page')
      ->setComponent($field_name, [
        'type' => 'autocomplete_mixed_matching',
        'settings' => [
          'match_limit' => 1,
        ],
      ])
      ->save();

    $this->doAndAssertNodeAutocomplete($field_name, [
      'Frog',
    ]);

    // Change the size of the result set via the UI.
    $this->drupalLogin($this->drupalCreateUser([
      'access content',
      'administer content types',
      'administer node fields',
      'administer node form display',
      'create page content',
    ]));
    $this->drupalGet('/admin/structure/types/manage/page/form-display');
    $assert_session->pageTextContains('Autocomplete suggestion list size: 1');
    // Click on the widget settings button to open the widget settings form.
    $this->submitForm([], $field_name . "_settings_edit");
    $this->assertSession()->waitForElement('css', sprintf('[name="fields[%s][settings_edit_form][settings][match_limit]"]', $field_name));
    $page->fillField('Number of results', '3');
    $page->pressButton('Save');
    $assert_session->pageTextContains('Your settings have been saved.');
    $assert_session->pageTextContains('Autocomplete suggestion list size: 3');

    $this->doAndAssertNodeAutocomplete($field_name, [
      'Frog',
      'Frog with a funny hat',
      '007 frog',
    ]);
  }

  /**
   * Runs the autocomplete, and asserts the matches.
   *
   * @param string $field_name
   *   Entity reference field name.
   * @param list<string> $expected
   *   Expected autocomplete matches.
   */
  protected function doAndAssertNodeAutocomplete(string $field_name, array $expected): void {
    $this->drupalGet('node/add/page');
    $this->doAutocomplete($field_name);
    $this->assertAutocompleteMatches($expected);
  }

  /**
   * Executes an autocomplete on a given field and waits for it to finish.
   *
   * @param string $field_name
   *   The field name.
   */
  protected function doAutocomplete(string $field_name): void {
    $autocomplete_field = $this->getSession()->getPage()->findField($field_name . '[0][target_id]') ?? $this->fail();
    $autocomplete_field->setValue('Frog');
    $this->getSession()->getDriver()->keyDown($autocomplete_field->getXpath(), ' ');
    $this->assertSession()->waitOnAutocomplete();
  }

  /**
   * Asserts that autocomplete matches are as expected.
   *
   * @param list<string> $expected
   *   Expected matches.
   */
  protected function assertAutocompleteMatches(array $expected): void {
    $page = $this->getSession()->getPage();
    $results = $page->findAll('css', '.ui-autocomplete li');
    $matches = \array_map(
      fn (NodeElement $element) => $element->getText(),
      $results,
    );
    // Unlike the core test, this one is sensitive to order of matches.
    $this->assertSame($expected, $matches);
  }

  /**
   * {@inheritdoc}
   */
  protected function drupalCreateUser(array $permissions = [], $name = NULL, $admin = FALSE, array $values = []): User {
    $user = parent::drupalCreateUser($permissions, $name, $admin, $values);
    $this->assertNotFalse($user);
    return $user;
  }

}
