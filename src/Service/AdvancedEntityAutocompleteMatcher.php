<?php

declare(strict_types=1);

namespace Drupal\autocomplete_mixed_matching\Service;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Entity\EntityAutocompleteMatcherInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;

/**
 * Replacement for EntityAutocompleteMatcher from Drupal core.
 *
 * This version supports an additional autocomplete operator, that is used if
 * the main operator does not produce enough results.
 *
 * The common way to use this would be with 'STARTS_WITH' as the main operator,
 * and 'CONTAINS' as the fallback operator.
 *
 * @see \Drupal\Core\Entity\EntityAutocompleteMatcher
 */
class AdvancedEntityAutocompleteMatcher implements EntityAutocompleteMatcherInterface {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selectionManager
   *   The entity reference selection handler plugin manager.
   */
  public function __construct(
    protected readonly SelectionPluginManagerInterface $selectionManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getMatches($target_type, $selection_handler, $selection_settings, $string = ''): array {
    if ($string === NULL) {
      // This should normally not happen because the default value is ''.
      // We still keep it, to be fully aligned with the core implementation.
      return [];
    }

    $options = $selection_settings + [
      'target_type' => $target_type,
      'handler' => $selection_handler,
    ];
    $handler = $this->selectionManager->getInstance($options);
    assert($handler instanceof SelectionInterface);

    $entity_labels_by_id = $this->getEntityLabelsById(
      $handler,
      $string,
      $selection_settings,
    );

    return $this->buildMatches($entity_labels_by_id);
  }

  /**
   * Finds matching entities using a fallback chain of match operators.
   *
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $handler
   *   Entity reference selection handler.
   * @param string $string
   *   Search string for autocomplete.
   * @param array $selection_settings
   *   Settings for the selection handler.
   *
   * @return array<int, string|\Drupal\Component\Render\MarkupInterface>
   *   Entity labels by entity id.
   */
  protected function getEntityLabelsById(SelectionInterface $handler, string $string, array $selection_settings): array {
    $match_operator = ($selection_settings['match_operator'] ?? NULL) ?: 'CONTAINS';
    $match_limit = (int) ($selection_settings['match_limit'] ?? 10);

    // Find matches for the first (or the only) operator.
    $entity_labels_by_id = $this->doGetEntityLabelsById(
      $handler,
      $string,
      $match_operator,
      $match_limit,
    );

    if (count($entity_labels_by_id) >= $match_limit) {
      // Enough matches were found with the first match operator.
      return $entity_labels_by_id;
    }

    $fallback_match_operator = $selection_settings['fallback_match_operator'] ?? NULL;
    if (!$fallback_match_operator) {
      // No alternative match operator was specified.
      return $entity_labels_by_id;
    }

    $entity_labels_by_id += $this->doGetEntityLabelsById(
      $handler,
      $string,
      $fallback_match_operator,
      $match_limit,
    );

    $entity_labels_by_id = array_slice($entity_labels_by_id, 0, $match_limit, TRUE);

    return $entity_labels_by_id;
  }

  /**
   * Finds matching entities using a specific match operator.
   *
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionInterface $handler
   *   Entity reference selection handler.
   * @param string $string
   *   Search string for autocomplete.
   * @param string $match_operator
   *   Autocomplete match operator, e.g. 'CONTAINS'.
   * @param int $match_limit
   *   Maximum number of results.
   *
   * @return array<int, string|\Drupal\Component\Render\MarkupInterface>
   *   Entity labels by entity id.
   */
  protected function doGetEntityLabelsById(SelectionInterface $handler, string $string, string $match_operator, int $match_limit): array {
    $entity_labels_by_bundle = $handler->getReferenceableEntities($string, $match_operator, $match_limit);

    $entity_labels_by_id = [];
    foreach ($entity_labels_by_bundle as $bundle_entity_labels_by_id) {
      $entity_labels_by_id += $bundle_entity_labels_by_id;
    }

    return $entity_labels_by_id;
  }

  /**
   * Builds autocomplete matches from entity labels.
   *
   * @param array<int, string|\Drupal\Component\Render\MarkupInterface> $entity_labels_by_id
   *   Entity labels by entity id.
   *
   * @return list<array{value: string, label: string|\Drupal\Component\Render\MarkupInterface}>
   *   Autocomplete matches.
   */
  protected function buildMatches(array $entity_labels_by_id): array {
    $matches = [];
    foreach ($entity_labels_by_id as $entity_id => $label) {
      $key = "$label ($entity_id)";
      // Strip things like starting/trailing white spaces, line breaks and
      // tags.
      $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))))
        ?? throw new \RuntimeException('Unexpected error in preg_replace().');
      // Names containing commas or quotes must be wrapped in quotes.
      $key = Tags::encode($key);
      $matches[] = ['value' => $key, 'label' => $label];
    }

    return $matches;
  }

}
