<?php

declare(strict_types=1);

namespace Drupal\autocomplete_mixed_matching\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Autocomplete with a combination of STARTS_WITH and CONTAINS matching.
 *
 * @FieldWidget(
 *   id = "autocomplete_mixed_matching",
 *   label = @Translation("Autocomplete, mixed matching"),
 *   description = @Translation("Autocomplete text field with combination of
 *   'starts with' and 'contains' matching."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 *
 * @see \Drupal\autocomplete_mixed_matching\Service\AdvancedEntityAutocompleteMatcher
 */
class MixedMatchingAutocompleteWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);
    // The match operator is fixed in this version.
    unset($form['match_operator']);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    try {
      // Temporarily set a value for 'match_operator', then unset it again.
      // Otherwise the parent method won't work correctly.
      $this->settings['match_operator'] = 'CONTAINS';
      $summary = parent::settingsSummary();
    }
    finally {
      unset($this->settings['match_operator']);
    }
    // Remove the first item which always contains the 'match_operator' setting.
    array_shift($summary);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $defaults = parent::defaultSettings();
    unset($defaults['match_operator']);
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $elements = parent::formElement($items, $delta, $element, $form, $form_state);
    assert(is_array($elements['target_id']['#selection_settings'] ?? NULL));
    $elements['target_id']['#selection_settings']['match_operator'] = 'STARTS_WITH';
    $elements['target_id']['#selection_settings']['fallback_match_operator'] = 'CONTAINS';
    return $elements;
  }

}
